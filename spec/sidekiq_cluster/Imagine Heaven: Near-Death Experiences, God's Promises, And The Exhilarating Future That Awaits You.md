## Imagine Heaven: Near-Death Experiences, God's Promises, And The Exhilarating Future That Awaits You

 
 ![Imagine Heaven: Near-Death Experiences, God's Promises, And The Exhilarating Future That Awaits You](https://media2.foxnews.com/BrightCove/694940094001/2016/01/02/694940094001_4683040852001_e9ec2fec-b1a7-486a-9a7f-fba06235e736.jpg)
 
 
**## Files you can download:
[Link 1](https://miimms.com/2tD8Ox)

[Link 2](https://gohhs.com/2tD8Ox)

[Link 3](https://urlcod.com/2tD8Ox)

**

 
 
 
 
 
# What Near-Death Experiences Can Teach Us About Heaven
 
Have you ever wondered what heaven is like? What happens when we die? Is there life after death? These are some of the most profound questions that humans have asked throughout history. And while no one can know for sure what lies beyond this world, some people claim to have glimpsed it through near-death experiences (NDEs).
 
NDEs are events that occur when a person is clinically dead or close to death, but then returns to life. They often involve sensations of leaving the body, seeing a bright light, meeting deceased loved ones, encountering spiritual beings, and feeling peace, love, and joy. Some NDEs also include visions of hell, judgment, or a life review.
 
According to a Gallup poll, about 15% of Americans have had an NDE or know someone who has. Many of them report that their NDE changed their lives for the better. They became more compassionate, grateful, spiritual, and hopeful. They also lost their fear of death and gained a new perspective on their purpose and destiny.
 
But what can we learn from NDEs about heaven? How do they relate to God's promises and the exhilarating future that awaits us? In his bestselling book *Imagine Heaven: Near-Death Experiences, God's Promises, and the Exhilarating Future That Awaits You*, pastor and author John Burke explores these questions by comparing hundreds of NDE stories with what the Bible says about heaven.
 
Burke argues that NDEs are not hallucinations, delusions, or wishful thinking. Rather, they are glimpses of the reality that God has prepared for those who love him. He shows how NDEs reveal aspects of heaven that are consistent with Scripture, such as:
 
- The beauty and majesty of God's creation
- The presence and glory of God and Jesus
- The love and joy of being with God and others
- The healing and restoration of our bodies and souls
- The rewards and responsibilities of our heavenly roles
- The freedom and adventure of exploring the new earth
- The justice and mercy of God's judgment

Burke also addresses some of the common questions and objections that people have about NDEs and heaven, such as:

- Are NDEs compatible with Christian doctrine?
- How can NDEs explain different religious experiences?
- How can we trust NDEs when they vary in details?
- How can we prepare for heaven and live in light of eternity?

If you are curious about heaven and want to discover more about what God has in store for you, *Imagine Heaven* is a must-read book. It will inspire you to live with hope, faith, and love in this life and the next.
  
## What is Heaven Like?
 
One of the most common questions that people have about heaven is what it is like. What will we see, hear, feel, and do there? How will we relate to God and others? What will our bodies and minds be like?
 
While the Bible does not give us a detailed description of heaven, it does give us some clues and hints. For example, it tells us that heaven is a place where God dwells with his people (Revelation 21:3), where there is no more death, sorrow, pain, or evil (Revelation 21:4), where there is fullness of joy and pleasures forevermore (Psalm 16:11), and where we will see God face to face and know him as we are known (1 Corinthians 13:12).
 
NDEs also give us some glimpses of what heaven is like. Many NDErs describe heaven as a place of indescribable beauty, light, color, and music. They say that heaven is filled with love, peace, and joy that surpasses anything they have ever experienced on earth. They also say that heaven is a place of learning, growth, and service, where we will use our gifts and talents to glorify God and bless others.
 
Of course, both the Bible and NDEs are limited in their ability to convey the reality of heaven. As the apostle Paul wrote, "No eye has seen, no ear has heard, and no mind has imagined what God has prepared for those who love him" (1 Corinthians 2:9). Heaven is beyond our human comprehension and imagination. But we can be sure that it is more wonderful than anything we can think or dream of.
  
## Why Does Heaven Matter?
 
Another question that people have about heaven is why it matters. How does knowing about heaven affect our lives here and now? What difference does it make to believe in heaven and hope for it?
 
The answer is that heaven matters a lot. Heaven is not just a distant destination that we will reach someday. It is also a present reality that influences our choices, attitudes, and actions today. As Jesus taught us to pray, "Your kingdom come, your will be done, on earth as it is in heaven" (Matthew 6:10). Heaven is the ultimate goal and standard of God's kingdom, which he invites us to participate in and anticipate now.
 
Knowing about heaven helps us to live with hope in the midst of suffering and trials. It reminds us that this world is not our home and that we have a better inheritance waiting for us (Hebrews 11:13-16). It also motivates us to endure hardship and persecution for the sake of Christ and his gospel (2 Corinthians 4:16-18).
 
Knowing about heaven also helps us to live with purpose and passion in our daily lives. It challenges us to seek first God's kingdom and righteousness (Matthew 6:33) and to store up treasures in heaven rather than on earth (Matthew 6:19-21). It also inspires us to use our time, talents, and resources to serve God and others (Matthew 25:31-46).
 
In short, knowing about heaven helps us to live with an eternal perspective that transforms our present reality. As C.S. Lewis wrote, "Aim at heaven and you will get earth thrown in. Aim at earth and you get neither."
 dde7e20689
 
 
